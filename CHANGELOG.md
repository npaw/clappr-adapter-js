## [6.7.2] - 2021-02-15
### Fixed
- Jointime detection for some cases with events triggered before it actually starts
- Mixed buffer seek event reporting

## [6.7.1] - 2021-02-09
### Added
- Seek buffer detection rework, using events instead of monitor
- GetPlayhead refactor
### Library
- Packaged with `lib 6.7.27`

## [6.7.0] - 2020-03-20
### Library
- Packaged with `lib 6.7.0`

## [6.3.0] - 2018-06-29
### Added
- Controlled ad errors
- Fixed seek report for some cases
### Library
- Packaged with `lib 6.3.0`

## [6.2.1] - 2018-05-23
### Added
- Ads Adapter for preroll plugin
### Library
- Packaged with `lib 6.2.6`

## [6.2.0] - 2018-04-10
### Library
- Packaged with `lib 6.2.2`
