/* global Clappr, Data */
var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.Clappr = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    var ret = 0
    if (this.flags.isJoined) {
      ret = this.player.getCurrentTime()
      if (this.plugin.getIsLive()){
         ret += this.player.getStartTimeOffset()
      }
    }
    return ret
  },

  /** Override to return video duration */
  getDuration: function () {
    if (this.getContainer().getPlaybackType() === 'vod' && this.player.getDuration() === 0) {
      return null
    }
    return this.player.getDuration()
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    return this.bitrate || -1
  },

  /** Override to return rendition */
  getRendition: function () {
    return this.rendition
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    if (this.getContainer().getPlaybackType() === 'vod' && this.player.getDuration() === 0) {
      return null
    }
    return this.getContainer().getPlaybackType() === 'live'
  },

  /** Override to return resource URL. */
  getResource: function () {
    var src = this.getPlayback().options.sources
    return (typeof src === 'object') ? src[0] : src
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return typeof Clappr !== 'undefined' ? Clappr.version : null
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Clappr'
  },

  /** Returns the container of the player */
  getContainer: function () {
    return this.player.core.getCurrentContainer()
  },

  /** Returns the playback of the player */
  getPlayback: function () {
    return this.player.core.getCurrentPlayback()
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    var events = Clappr.Events
    this.playbackReferences = {}
    this.playbackReferences[events.PLAYBACK_PLAY_INTENT] = this.playIntentListener.bind(this)
    this.playbackReferences[events.PLAYBACK_PLAY] = this.playListener.bind(this)
    this.playbackReferences[events.PLAYBACK_PAUSE] = this.pauseListener.bind(this)
    this.playbackReferences[events.PLAYBACK_STOP] = this.stopListener.bind(this)
    this.playbackReferences[events.PLAYBACK_ENDED] = this.stopListener.bind(this)
    this.playbackReferences[events.PLAYBACK_ERROR] = this.errorListener.bind(this)
    this.playbackReferences[events.PLAYBACK_BITRATE] = this.bitrateListener.bind(this)
    this.playbackReferences[events.PLAYBACK_PROGRESS] = this.timeupdateListener.bind(this)
    this.playbackReferences[events.PLAYBACK_SEEK] = this.seekListener.bind(this)
    this.playbackReferences[events.PLAYBACK_SEEKED] = this.seekedListener.bind(this)
    this.playbackReferences[events.PLAYBACK_BUFFERING] = this.bufferingListener.bind(this)

    this.containerReferences = {}
    this.containerReferences[events.CONTAINER_SEEK] = this.seekListener.bind(this)
    if (this.getPlayback()) {
      for (var key in this.playbackReferences) {
        this.getPlayback().on(key, this.playbackReferences[key])
      }
    }

    if (this.getContainer()) {
      for (var key2 in this.containerReferences) {
        this.getContainer().on(key2, this.containerReferences[key2])
      }
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // unregister listeners
    if (this.getPlayback() && this.playbackReferences) {
      for (var key in this.playbackReferences) {
        this.getPlayback().off(key, this.playbackReferences[key])
      }
      this.playbackReferences = {}
    }
    if (this.getContainer() && this.containerReferences) {
      for (var key2 in this.containerReferences) {
        this.getContainer().off(key2, this.containerReferences[key2])
      }
      this.containerReferences = {}
    }
  },

  timeupdateListener: function (e) {
    if (!this.eventStop) {
      if (!this.isUsingPrerollPlugin()) {
        this.fireStart()
      }
      if (this.getPlayhead() > 0.1) {
        this._fireProtectedJoin()
      }
    }
  },

  /** Listener for 'play intent' event. */
  playIntentListener: function () {
    this.fireStart()
    this.eventStop = false
    this.tag = this.getPlayback().video
  },

  /** Listener for 'play' event. */
  playListener: function () {
    this.eventStop = false
    this.fireResume()
    this.fireSeekEnd()
    this.fireBufferEnd()
    this._fireProtectedJoin()
    this.fireStart()
  },

  /** Listener for 'pause' event. */
  pauseListener: function () {
    this.firePause()
  },

  /** Listener for 'stop' and 'ended' events. */
  stopListener: function (e) {
    if (!this.plugin.getAdsAdapter() || !this.isUsingPrerollPlugin() || (this.plugin.getAdsAdapter() && !this.plugin.getAdsAdapter().flags.isStarted)) {
      this.eventStop = true
      this.fireStop()
    }
  },

  /** Listener for 'error' event. */
  errorListener: function (error, player) {
    var now = new Date().getTime()
    if (!this.lastErrorTime || now - Data.this.lastErrorTime > 1000) {
      var errorObject = error.data || error.raw
      if (errorObject) {
        this.fireError(errorObject.type, errorObject.details)
        if (errorObject.fatal) {
          this.eventStop = true
          this.fireStop()
        }
      } else {
        if (this.isUsingPrerollPlugin()) {
          this.registerListeners()
        } else {
          this.fireError(error.code, error.message)
        }
      }
      this.lastErrorTime = now
    }
  },

  /** Listener for 'bitrate' event. */
  bitrateListener: function (e) {
    this.bitrate = e.bitrate
    if (e.bandwidth && e.width && e.height) {
      this.rendition = youbora.Util.buildRenditionString(e.width, e.height, e.bandwidth)
    }
  },

  seekListener: function (e) {
    this.fireSeekBegin()
  },

  seekedListener: function (e) {
    if (!this.flags.isBuffering){
      this.fireSeekEnd()
    }
  },

  bufferingListener: function (e) {
    if (!this.flags.isSeeking) {
      this.fireBufferBegin()
    }
  },

  _fireProtectedJoin: function(e) {
    if (this.player.isPlaying()) {
      this.fireJoin()
    }
  },

  isUsingPrerollPlugin: function () {
    return this.plugin.getAdsAdapter() && this.plugin.getAdsAdapter().isPrerollPlugin && this.plugin.getAdsAdapter().isPrerollPlugin()
  }
},
// Static members
{
  PrerollAdsAdapter: require('./ads/prerollAdsAdapter')
}
)

module.exports = youbora.adapters.Clappr
