var youbora = require('youboralib')
var manifest = require('../../manifest.json')

var PrerollAdsAdapter = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-prerollHtml5-ads'
  },

  getTitle: function () {
    return 'unknown'
  },

  /** Override to return video duration */
  getDuration: function () {
    return 0
  },

  /** Override to return resource URL. */
  getResource: function () {
    return 'unknown'
  },

  /** Override to return current ad position (only ads) */
  getPosition: function () {
    return 'pre'
  },

  eventListeners: function (e) {
    var context = this
    return {
      content_resume_requested: function () {
        context.fireStop()
        context.plugin.getAdapter().fireResume()
      },
      content_pause_requested: function () {
        context.plugin.fireInit()
        context.fireStart()
        context.plugin.getAdapter().firePause()
      },
      loaded: function () {
        context.player = context.plugin.getAdapter().player
      },
      click: function () {
        context.fireClick()
      },
      started: function () {
        context.fireJoin()
      },
      complete: function () {
        context.fireStop()
      },
      paused: function () {
        context.firePause()
      },
      resumed: function () {
        context.fireResume()
      },
      skipped: function () {
        context.fireStop({ skipped: true })
      },
      error: function () {
        context.fireError()
        context.fireStop()
      },
      ad_error: function () {
        context.fireError()
        context.fireStop()
      }
    }
  },

  isPrerollPlugin: function () {
    return true
  }
})

module.exports = PrerollAdsAdapter
